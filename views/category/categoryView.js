console.log("categoryView.js loaded")

const electron = require('electron').remote
const {app, BrowserWindow, Menu} = electron
const path = require('path')
const url = require('url')
const fs = require('fs')

let win = electron.getCurrentWindow();
let currentProject = electron.getGlobal('sharedObject').currentProject;

(function(){
    categoriesLoad();
    document.querySelector('.app-breadcrumb small').innerHTML = currentProject+" > Categories"
})();

win.on('focus', function () {
    categoriesLoad();
});

win.on('show', function() {
    categoriesLoad();
});

win.once('ready-to-show', () => {
    win.show();
    categoriesLoad();
});

function clickGoBack()
{
    win.loadURL(url.format({
          pathname: path.join(__dirname, '../main/mainView.html'),
          protocol: 'file:',
          slashes: true
    }));
}


function categoriesLoad()
{
    var categoryNames = [];

    var dir = electron.getGlobal('constants').appFolder+'/projects/'+currentProject;

    var files = fs.readdirSync(dir);

    try{

        if(fs.lstatSync(dir).isDirectory())
        {
            var files = fs.readdirSync(dir);

            for(var i = 0; i < files.length; i++)
            {
                var file = files[i];
                var filePath = dir+file;
                var ext = getExtensionFromString(filePath);

                if(file != '.DS_Store' && file != '.' && file != '..' && ext == 'json')
                {
                    var txt = JSON.parse(fs.readFileSync(filePath, 'utf-8'));

                    categoryNames.push(txt['project-name']);
                }
            }

            categoriesListInsert(categoryNames);
        }
    }
    catch(e)
    {
        console.log(e);
    }

    categoriesListInsert(categoryNames);
}

function categoriesListInsert(categoryNames)
{
    document.getElementById('project-categories-list').innerHTML = "";

    for(var i = 0; i < categoryNames.length; i++)
    {
         document.getElementById('project-categories-list').innerHTML +=
            '<div class="row list-element-name task" data-id="'+categoryNames[i]+'">'+
                '<div class="col-12">'+
                    '<span>'+
                        categoryNames[i]+
                    '</span>'+
                '</div>'+
            '</div>';
    }
}

document.querySelector("#go-to-main").addEventListener('click', clickGoBack);

function getExtensionFromString(filePath) {
   return filePath.substring(filePath.lastIndexOf(".")+1)
}
