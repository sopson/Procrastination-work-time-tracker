console.log("addProjectView.js loaded")

const electron = require('electron').remote
const {app} = electron;

const path = require('path')
const fs = require('fs')

let win = electron.getCurrentWindow();

function saveProjectButtonClick()
{
    var fields = document.getElementsByClassName("form-control");
    var saveData = {};

    for(var i = 0; i < fields.length; i++) {
        saveData[fields[i].name] = fields[i].value;
    }

    saveProjectToFile(saveData);
}

function saveProjectToFile(data)
{
    var dir = electron.getGlobal('constants').appFolder;

    checkAndCreateFolder(dir);

    dir += '/projects/';

    checkAndCreateFolder(dir);
    checkAndCreateFolder(dir+data['project-name']);

    var files = fs.readdirSync(dir);

    var filePath = dir+files.length+" "+data['project-name']+'.json';
    var json = JSON.stringify(data);

    fs.writeFile(filePath, json, function (err) {
        if (err === undefined || err == null) {
            win.close();
        }
        else {
            alert(err);
        }
    });
}

function checkAndCreateFolder(dir)
{
    try
    {
        fs.lstatSync(dir).isDirectory();
    }
    catch (e)
    {
        if(e.code == 'ENOENT')
        {
            fs.mkdir(dir);
        }
        else
        {
            console.log('checkAndCreateFolder(dir):');
            console.log(e);
        }
    }
}

document.querySelector("#save-project-button").addEventListener('click', saveProjectButtonClick);
