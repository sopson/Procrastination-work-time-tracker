console.log("mainView.js loaded")

const electron = require('electron').remote
const {app, BrowserWindow, Menu} = electron
const path = require('path')
const url = require('url')
const fs = require('fs')

let addProjectWindow;
let win = electron.getCurrentWindow();

let contextProjectID;

(function() {
   projectsLoad();
})();

win.on('focus', function () {
    projectsLoad();
});

win.on('show', function() {
    projectsLoad();
});

win.once('ready-to-show', () => {
    win.show();
    projectsLoad();
});

function openAddProjectWindow()
{
    if (addProjectWindow) {
        addProjectWindow.focus()
        return;
    }

    addProjectWindow = new BrowserWindow({
        title: 'Add project',
        width: 500,
        height: 600,
        resizable: false,
        minimizable: false,
        fullscreenable: false,
        parent: win,
    });

    addProjectWindow.loadURL(url.format({
          pathname: path.join(__dirname, '../addProject/addProjectView.html'),
          protocol: 'file:',
          slashes: true
    }));

    addProjectWindow.on('close', function() {
        addProjectWindow = null;
        projectsLoad();
    });
};

function projectsListInsert(projectNames, projectRate, projectCurrency)
{
    document.getElementById('projects-list').innerHTML = "";

    for(var i = 0; i < projectNames.length; i++)
    {
         document.getElementById('projects-list').innerHTML +=
            '<div class="row list-element-name task" data-id="'+projectNames[i]+'">'+
                '<div class="col-8">'+
                    '<span>'+
                        projectNames[i]+
                    '</span>'+
                '</div>'+
                '<div class="col-4 text-right">'+
                    '<span>'+
                        projectRate[i]+" "+projectCurrency[i]+
                        '/h'+
                    '<span>'+
                '</div>'+
            '</div>';
    }

    var projects = document.getElementsByClassName("task");

    for(var i = 0; i < projects.length; i++)
    {
        projects[i].addEventListener('click', openSpecificProject);
        projects[i].addEventListener('contextmenu', function(e) {
            contextProjectID = clickInsideElement( e, 'task' );
        });
    }
}

function projectsLoad()
{
    var projectNames = [];
    var projectRate = [];
    var projectCurrency = [];

    var dir = electron.getGlobal('constants').appFolder+'/projects/';

    try{

        if(fs.lstatSync(dir).isDirectory())
        {
            var files = fs.readdirSync(dir);

            for(var i = 0; i < files.length; i++)
            {
                var file = files[i];
                var filePath = dir+file;
                var ext = getExtensionFromString(filePath);

                if(file != '.DS_Store' && file != '.' && file != '..' && ext == 'json')
                {
                    var txt = JSON.parse(fs.readFileSync(filePath, 'utf-8'));

                    projectNames.push(txt['project-name']);
                    projectRate.push(txt['project-hourly-rate']);
                    projectCurrency.push(txt['project-currency']);
                }
            }

            projectsListInsert(projectNames, projectRate, projectCurrency);
        }
    }
    catch(e)
    {
        console.log(e);
    }
}

document.querySelector("#add-button").addEventListener('click', openAddProjectWindow);

function openSpecificProject(e = null, dataProject = "")
{
    if(e!=null && dataProject == "")
    {
       dataProject = clickInsideElement(e, 'task').getAttribute('data-id');
    }

    electron.getGlobal('sharedObject').currentProject = dataProject;
    console.log(electron.getGlobal('sharedObject'));

    win.loadURL(url.format({
          pathname: path.join(__dirname, '../category/categoryView.html'),
          protocol: 'file:',
          slashes: true
    }));
}

function clickInsideElement( e, className ) {
    var el = e.srcElement || e.target;

    if ( el.classList.contains(className) ) {
        return el;
    }
    else
    {
        while ( el = el.parentNode )
        {
            if ( el.classList && el.classList.contains(className) )
            {
                return el;
            }
        }
    }

    return false;
}

function setContextMenuActions(e)
{
    var contextAction = clickInsideElement(e,'context-menu__link').getAttribute("data-action");

    if(!contextAction) {
        contextAction = "view";
    }

    doProjectAction(contextAction);
}

function doProjectAction(action = "view")
{
    switch (action) {
        case "View":
            openSpecificProject(null, contextProjectID.getAttribute("data-id"));
        break;
        case "Edit":
        // TODO:
        break;
        case "Delete":
        // TODO:
        break;
        default:

    }
}

var contextActions = document.getElementsByClassName("context-menu__link");

for(var i = 0; i < contextActions.length; i++)
{
    contextActions[i].addEventListener('click', setContextMenuActions);
}

function getExtensionFromString(filePath) {
   return filePath.substring(filePath.lastIndexOf(".")+1)
}
