const electron = require('electron')
const {
    app,
    BrowserWindow,
    Menu,
    Tray
} = electron

const path = require('path')
const url = require('url')
const fs = require('fs')
const nativeImage = electron.nativeImage

require('electron-reload')(__dirname)

let win
let tray = null

let addProjectWindow

process.env.NODE_ENV = 'development';

global.constants = {
    appFolder: app.getPath('appData')+'/procrastination-work-time-tracker'
}

app.on('ready', function()
{
    createMainWindow();
    createTrayIcon();
    createMainMenu();

    global.sharedObject = {
        currentProject: null,
        currentCategory: null,
        currentTask: null,
        coreAppTray: tray,
    };
});

function createMainWindow()
{
    const {width, height} = electron.screen.getPrimaryDisplay().workAreaSize

    const appPosX = (width-625);
    const appPosY = process.platform === 'darwin' ? 0 : height;

    win = new BrowserWindow({
        title: 'Procrastination work time tracker',
        frame: false,
        width: 500,
        height: 600,
        x: appPosX,
        y: appPosY,
        resizable: false,
        minimizable: false,
        fullscreenable: false,
        show: false,
        icon: path.join(__dirname, 'assets/img/icon/linux/logo.png')
    })

    win.loadURL(url.format({
        pathname: path.join(__dirname, 'views/main/mainView.html'),
        protocol: 'file:',
        slashes: true
    }))

    win.webContents.openDevTools()

    win.on('closed', () => {
        win = null;
    })
}

function createTrayIcon()
{
    var image = nativeImage.createFromPath('./assets/img/tray/macos/trayTemplate.png');

    image.setTemplateImage(true);

    tray = new Tray(image);
    tray.setToolTip("No running tasks");
    tray.on('click', function() {
        if(!win.isVisible()) {
            win.show();
            win.focus();
        }
        else {
            win.focus();
        }
    });
}

const MainMenuTemplate = [
{
    label: 'File',
    submenu: [{
        label: 'Preferences',
        accelerator: process.platform === 'darwin' ? 'Command+,' : 'Ctrl+P',
        cilick() {
            console.log("Show Settings");
        }
    },
    {
        label: 'Export',
        cilick() {
            console.log("Export");
        }
    },
    {
        label: 'Import',
        cilick() {
            console.log("Import");
        }
    },
    {
        label: 'Credits',
        accelerator: process.platform === 'darwin' ? 'Command+F1' : 'F1',
        click() {
            console.log("Show Credits");
        }
    },
    {
        label: 'Hide',
        accelerator: process.platform === 'darwin' ? 'Command+H' : 'Ctrl+H',
        click() {
            app.hide();
        }
    },
    {
        label: 'Exit',
        accelerator: process.platform === 'darwin' ? 'Command+Q' : 'Ctrl+Q',
        click() {
            app.quit();
        }
    }]
},
{
    label: 'Edit',
    submenu: [{
        label: 'Undo',
        accelerator: 'CmdOrCtrl+Z',
        selector: 'undo:'
    },
    {
        label: 'Redo',
        accelerator: 'Shift+CmdOrCtrl+Z',
        selector: 'redo:'
    },
    {
        type: 'separator'
    },
    {
        label: 'Cut',
        accelerator: 'CmdOrCtrl+X',
        selector: 'cut:'
    },
    {
        label: 'Copy',
        accelerator: 'CmdOrCtrl+C',
        selector: 'copy:'
    },
    {
        label: 'Paste',
        accelerator: 'CmdOrCtrl+V',
        selector: 'paste:'
    },
    {
        label: 'Select All',
        accelerator: 'CmdOrCtrl+A',
        selector: 'selectAll:'
    }]
}]

function createMainMenu()
{
    const mainMenu = Menu.buildFromTemplate(MainMenuTemplate);
    Menu.setApplicationMenu(mainMenu);
}

// function openAddProjectWindow()
// {
//     if (addProjectWindow) {
//         addProjectWindow.focus()
//         return;
//     }
//
//     addProjectWindow = new BrowserWindow({
//         title: 'Add project',
//         width: 500,
//         height: 600,
//         resizable: false,
//         minimizable: false,
//         fullscreenable: false,
//         parent: win
//     });
//
//     addProjectWindow.loadURL(url.format({
//           pathname: path.join(__dirname, 'views/addProject/addProjectView.html'),
//           protocol: 'file:',
//           slashes: true
//     }))
//
//     addProjectWindow.on('close', function() {
//         addProjectWindow = null;
//     })
// };

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    if (win === null) {
        createWindow();
    }
})

if(process.env.NODE_ENV !== 'production') {
    MainMenuTemplate.push({
        label: 'Developer Tools',
        submenu:[
        {
            role: 'reload'
        },
        {
            label: 'Toggle DevTools',
            accelerator:process.platform == 'darwin' ? 'Command+I' : 'Ctrl+I',
            click(item, focusedWindow){
                focusedWindow.toggleDevTools();
            }
        }
        ]
    });
}
