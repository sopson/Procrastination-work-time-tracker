console.log("index.js loaded");

// Contains all universal codes
// Enables:
//     - tooltips

const tt = require('electron-tooltip');

tt({
    position: 'bottom',
    width: 250,
    offset: 5,
    style: {
        backgroundColor: '#3f3f3f',
        borderRadius: '4px',
        color: '#FFF'
    }
});
